/* eslint-disable import/prefer-default-export */
/* eslint-disable no-await-in-loop */
/* eslint-disable import/extensions */
import { performance } from 'perf_hooks';
import sharp from 'sharp';
import {
  CONCURRENCY_COUNT,
  OLD_S3_HOST,
  NEW_S3_HOST,
} from './config/config.js';

import {
  uploadImageToS3,
  resizeImage,
  getImageFromS3,
} from './util/resizeUtil.js';

async function updateNewUrl(obj, imageUrl) {
  const newUrl = imageUrl.replace(OLD_S3_HOST, NEW_S3_HOST);

  const imagesList = obj.images.filter((value) => value !== imageUrl);

  imagesList.push(newUrl);

  Object.assign(obj, { images: imagesList });

  await obj.save();
  return obj;
}

// process image in array of images
async function processMultiImage(obj) {
  const listImages = obj.images;

  for (let i = 0; i < listImages.length; i += 1) {
    const imageUrl = listImages[i];
    if (imageUrl.includes(OLD_S3_HOST)) {
      const imageName = imageUrl.replace(OLD_S3_HOST, '');

      try {
        const rawImageBuffer = await getImageFromS3(imageName);
        // outputBuffer contains image data
        // of at least 512 pixels wide and 512 pixels high while maintaining aspect ratio
        // and no smaller than the input image
        const options = {
          width: 512,
          height: 512,
          fit: sharp.fit.inside,
          withoutEnlargement: true,
        };
        const resizeImageBuffer = await resizeImage(rawImageBuffer, options);
        const result = await uploadImageToS3(imageName, resizeImageBuffer);
        if (result) {
          // eslint-disable-next-line no-param-reassign
          obj = await updateNewUrl(obj, imageUrl);
        }
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
      }
    }
  }
}

// create list promise that process the image in each object
async function processMultipleTask(listObjects) {
  if (Array.isArray(listObjects) && listObjects.length > 0) {
    const listPromises = listObjects.map((obj) => processMultiImage(obj));
    await Promise.all(listPromises);
  }
}

// get list object contains unresized images
async function getListObjects(Model) {
  return Model.find({
    images: new RegExp(OLD_S3_HOST, 'i'),
  }).limit(CONCURRENCY_COUNT);
}

// check if this model has any images that wasn't resized
async function checkExistsRawImages(Model) {
  return Model.exists({
    images: new RegExp(OLD_S3_HOST, 'i'),
  });
}

// function start
export const resizeObjectWithArrayImages = async (Model) => {
  const start = performance.now();
  while (await checkExistsRawImages(Model)) {
    const listObjects = await getListObjects(Model);
    await processMultipleTask(listObjects, Model);
  }
  const end = performance.now();
  // eslint-disable-next-line no-console
  console.log(`Time: ${end - start}ms`);
};
