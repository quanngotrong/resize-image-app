FROM node:14

WORKDIR /test-batch-job

COPY ./ /test-batch-job

RUN rm -rf node_modules

RUN npm i

EXPOSE 3000