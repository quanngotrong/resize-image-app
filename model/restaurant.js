import mongoose from 'mongoose';

const Restaurant = mongoose.model(
  'Restaurant',
  new mongoose.Schema({
    name: String,
    image: {
      type: String,
      trim: true,
    },
  }),
  'Restaurant',
);

export default Restaurant;
