import mongoose from 'mongoose';

const Material = mongoose.model(
  'materials',
  new mongoose.Schema({
    name: String,
    image: {
      type: String,
    },
  }),
  'materials',
);

export default Material;
