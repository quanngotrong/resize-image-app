import mongoose from 'mongoose';

const Movie = mongoose.model(
  'Movie',
  new mongoose.Schema({
    name: String,
    images: {
      type: [String],
    },
  }),
  'movies',
);

export default Movie;
