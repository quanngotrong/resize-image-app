import mongoose from 'mongoose';

const Dish = mongoose.model(
  'Dish',
  new mongoose.Schema({
    images: {
      type: [String],
    },
  }),
  'Dish',
);

export default Dish;
