/* eslint-disable import/extensions */
import mongoose from 'mongoose';
import Movie from './model/movie.js';
import Material from './model/material.js';
import Dish from './model/dish.js';
import Restaurant from './model/restaurant.js';

import { MONGODB_CONNECTION_STRING } from './config/config.js';

// use for model with array images
import { resizeObjectWithArrayImages } from './resizeArrayImage.js';
// use for model with single image
import { resizeObjectWithImage } from './resizeImage.js';
// connect to mongodb
async function connect() {
  await mongoose.connect(MONGODB_CONNECTION_STRING);
}

async function main() {
  await connect();
  resizeObjectWithImage(Restaurant);
  resizeObjectWithImage(Material);
  resizeObjectWithArrayImages(Dish);

  // test
  resizeObjectWithArrayImages(Movie);
}

try {
  main();
} catch (e) {
  // eslint-disable-next-line no-console
  console.error(e);
}
