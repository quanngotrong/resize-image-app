/* eslint-disable import/extensions */
import sharp from 'sharp';
import {
  OLD_BUCKET,
  NEW_BUCKET,
} from '../config/config.js';
import { createObject, getObject } from './AWSS3Provider.js';

export const getUploadParamsFromBuffer = async (fileName, buffer) => ({
  Bucket: NEW_BUCKET,
  Key: fileName,
  Body: buffer,
});

export const uploadImageToS3 = async (imageName, resizeImageBuffer) => {
  const file = await getUploadParamsFromBuffer(imageName, resizeImageBuffer);
  return createObject(file);
};

/**
 * Resize image
 * @param {*} imageBuffer
 * @param {*} options { width: <width>, height: <height>
 * @returns BufferImage
 */
export const resizeImage = async (imageBuffer, options) => {
  try {
    return await sharp(imageBuffer).resize(options).toBuffer();
  } catch {
    return null;
  }
};

// get image from s3
export const getImageFromS3 = async (imageName) => getObject({
  Bucket: OLD_BUCKET,
  Key: imageName,
});
