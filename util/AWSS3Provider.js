/* eslint-disable import/extensions */
import {
  PutObjectCommand,
  GetObjectCommand,
} from '@aws-sdk/client-s3';

import _ from 'lodash';
import { s3Client } from './AWSS3Client.js';
/**
 * Parameters sample
{
    Bucket: 'BUCKET_NAME', // The name of the bucket. For example, 'sample_bucket_101'.
    Key: 'KEY', // The name of the object. For example, 'sample_upload.txt'.
    Body: 'BODY', // The content of the object. For example, 'Hello world!".
};
*/

export const createObject = async (params) => {
  // Create an object and upload it to the Amazon S3 bucket.
  try {
    const results = await s3Client.send(new PutObjectCommand(params));

    if (_.get(results, '$metadata.httpStatusCode', 0) === 200) {
      // eslint-disable-next-line no-console
      console.log('Upload successfully');
    }
    return results; // For unit tests.
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('Error', err);
    return null;
  }
};

/**
 * @parameters
{
    Bucket: 'BUCKET_NAME', // The name of the bucket. For example, 'sample_bucket_101'.
    Key: 'KEY', // The name of the object. For example, 'sample_upload.txt'.
};
 * @returns Buffer
 */
export const getObject = async (params) => {
  // Create an object and upload it to the Amazon S3 bucket.
  try {
    // Create a helper function to convert a ReadableStream to a Buffer.
    const streamToBuffer = (stream) => new Promise((resolve, reject) => {
      const chunks = [];
      stream.on('data', (chunk) => chunks.push(chunk));
      stream.on('error', reject);
      stream.on('end', () => resolve(Buffer.concat(chunks)));
    });
    const result = await s3Client.send(new GetObjectCommand(params));
    // return result.Body;
    return await streamToBuffer(result.Body);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('Error', err);
    return null;
  }
};
