/* eslint-disable import/extensions */
import { S3Client } from '@aws-sdk/client-s3';
import { AWS_REGION } from '../config/config.js';

const config = {
  region: AWS_REGION,
};

// const config = {
//   region: AWS_REGION,
//   credentials:{
//       accessKeyId:'',
//       secretAccessKey:''
//   }
// }

const s3Client = new S3Client(config);
// eslint-disable-next-line import/prefer-default-export
export { s3Client };
