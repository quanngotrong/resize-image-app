# Script to download, resize and upload image to AWS S3
- Set AWS credentials in environment
    + export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
    + export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>

resizeArrayImage.js : resize images in model with array of images
resizeImage.js : resize image in model with single image

steps:
- change file config/config.js
- define model in model folder
- change the main function in index.js

run: 
node index.js